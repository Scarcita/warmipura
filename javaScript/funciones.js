
    var form = document.getElementById("my-form");
    
    async function handleSubmit(event) {
      event.preventDefault();
      var status = document.getElementById("status");
      var data = new FormData(event.target);
      fetch(event.target.action, {
        method: form.method,
        body: data,
        headers: {
            'Accept': 'application/json'
        }
      }).then(response => {
        if (response.ok) {
          status.innerHTML = "Formulario enviado, Muchas gracias!";
          status.classList.add('success')
          form.reset()
        } else {
          response.json().then(data => {
            if (Object.hasOwn(data, 'errors')) {
              status.innerHTML = data["errors"].map(error => error["message"]).join(", ")
              status.classList.add('error')
            } else {
              status.innerHTML = "Oops!Hubo un error"
            }
          })
        }
      }).catch(error => {
        status.innerHTML = "Oops! Hay un problema en el formulario"
      });
    }
    form.addEventListener("submit", handleSubmit)
